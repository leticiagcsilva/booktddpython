# Autores e Agradecimentos

Este livro tem e terá vários autores. Trata-se de um livro desenvolvido colaborativamente ao longo do tempo, profissionais responsáveis pela disciplina **ESBD2 - Programação e Desenvolvimento Dirigido por Testes**, ministrada no módulo do curso de [MBA Machine Learning in Production](https://iti.ufscar.mba/mlp) do [ITI-UFSCar](https://iti.ufscar.mba).&#x20;

O [Prof. Auri Vincenzi](http://lapes.dc.ufscar.br/members/faculties/auri-vincenzi) (DC/UFSCar) é o idealizador e coordenador, entretanto, são vários os colaboradores que permitiram a escrita do conteúdo aqui disponibilizado. Desde já agradeço o empenho de todos no desenvolvimento deste material. Sem vocês certamente este projeto não poderia ser realizado.

Pedro Sakuma Travi (Santander) possui vasta experiência no uso do TDD como metodologia de desenvolvimento e contribui extensivamente com as práticas de TDD e os relatos de experiência aqui apresentados.

Eduardo Molina (Raccoon) possui experiência em desenvolvimento Python e contribui com a melhoria, detalhamento das explicações de todo código Python presente neste material.&#x20;

Jasiel Josias Lima Macagnan (PPGCC/UFSCar) mestrando do PPGCC/UFSCar, atua na área de desenvolvimento de aplicações computacionais, com experiência em linguagens de programação como Java, Python, Javascript, C++, entre outras tecnologias da área.

A [Profa. Marilde Terezinha Prado Santos](http://www2.dc.ufscar.br/\~marilde/) (DC/UFSCar) é especialista em Educação a Distância, organizadora do MBA _Machine Learning in Production_ e contribui enormemente com o desenvolvimento do conteúdo aqui apresentado.

