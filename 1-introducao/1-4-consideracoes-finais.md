# 1.4 Considerações Finais

Neste capítulo apresentamos uma descrição geral e motivacional sobre o uso do Desenvolvimento Dirigido por Testes (TDD) na concepção de uma aplicação simples. Como observado, TDD tem tudo a ver com testes. Pensar em como validar um artefato em desenvolvimento faz com que o desenvolvedor tenha certeza do que o respectivo artefato deve fazer, ou seja, como ele deve se comportar ao receber entradas para serem processadas.

Nesse sentido, o capítulo a seguir apresenta alguns conceitos básicos sobre teste de software para auxiliar o desenvolvedor a elaborar um conjunto de teste consistente, e com base em critérios de teste, para iniciar o desenvolvimento de uma aplicação.
